import pygame
import sys
from bullet import Bullet
from asteroid import Asteroid
from time import sleep
from splash import Splash


def check_keydown_event(screen, ai_settings, gun, bullets, event, asteroids) -> None:
    if event.key == pygame.K_RIGHT:
        gun.rotate_direction = -1
    elif event.key == pygame.K_LEFT:
        gun.rotate_direction = 1
    elif event.key == pygame.K_q:
        sys.exit()
    elif event.key == pygame.K_SPACE:
        fire_bullet(screen, ai_settings, gun, bullets)
    elif event.key == pygame.K_w:
        gun.move_up = True
    elif event.key == pygame.K_a:
        gun.move_left = True
    elif event.key == pygame.K_s:
        gun.move_down = True
    elif event.key == pygame.K_d:
        gun.move_right = True


def check_keyup_event(event, gun) -> None:
    if event.key == pygame.K_RIGHT:
        gun.rotate_direction = 0
    elif event.key == pygame.K_LEFT:
        gun.rotate_direction = 0
    elif event.key == pygame.K_w:
        gun.move_up = False
    elif event.key == pygame.K_a:
        gun.move_left = False
    elif event.key == pygame.K_s:
        gun.move_down = False
    elif event.key == pygame.K_d:
        gun.move_right = False


def check_game_events(screen, ai_settings, ball, bullets, asteroids) -> None:
    """Ловим события нажатия на клавиши"""
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            check_keydown_event(screen, ai_settings, ball, bullets, event, asteroids)
        elif event.type == pygame.KEYUP:
            check_keyup_event(event, ball)


def update_bullets(screen, bullets, asteroids):
    for bullet in bullets.copy():
        if (bullet.rect.centery <= 10) or (bullet.rect.centery > screen.get_rect().height):
            bullets.remove(bullet)
        if (bullet.rect.centerx > screen.get_rect().width) or (bullet.rect.centerx <= 10):
            bullets.remove(bullet)

    collisions = pygame.sprite.groupcollide(asteroids, bullets, True, True)
    bullets.update()


def fire_bullet(screen, ai_settings, ball, bullets) -> None:
    new_bullet = Bullet(screen, ball, ai_settings)
    bullets.add(new_bullet)


def update_screen(screen, ai_settings, gun, bullets, asteroids) -> None:
    gun.rotate(screen, ai_settings)
    gun.moove(screen, ai_settings)

    for bullet in bullets:
        bullet.draw_bullet()

    for asteroid in asteroids:
        asteroid.draw_asteroid(screen)

    pygame.display.flip()
    pygame.time.delay(ai_settings.game_delay)


def gun_hit(gun, asteroids, screen):
    gun.centered()
    asteroids.empty()

    centerx = gun.rect.centerx
    centery = gun.rect.centery

    splash = Splash(centerx, centery)

    splash.draw_splash(screen)
    pygame.display.flip()
    sleep(0.5)


def update_asteroids(screen, asteroids, gun):
    for asteroid in asteroids.copy():
        if (asteroid.rect.centery <= -100) or (asteroid.rect.centery > screen.get_rect().height + 100):
            asteroids.remove(asteroid)
        if (asteroid.rect.centerx > screen.get_rect().width + 100) or (asteroid.rect.centerx <= -100):
            asteroids.remove(asteroid)
    asteroids.update()

    if pygame.sprite.spritecollideany(gun, asteroids):
        gun_hit(gun, asteroids, screen)


def create_asteroid(screen, ai_settings, asteroids):
    if len(asteroids) <= ai_settings.max_asteroids_amount:
        asteroid = Asteroid(screen)
        asteroids.add(asteroid)
