import pygame


class Gun:
    def __init__(self, screen) -> None:
        self.image = pygame.image.load('images/rocket_1.png').convert()
        # set_colorkey применяется только для непрозрачных фонов
        self.image.set_colorkey((255, 255, 255))
        self.new_image = self.image
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()
        self.move_up = False
        self.move_down = False
        self.move_left = False
        self.move_right = False
        self.x = self.screen_rect.centerx
        self.y = self.screen_rect.centery
        self.angle = 0
        self.rotate_direction = 0  # 0 - нет врещения, -1 - по часовой стр., 1 против часовой стр.

    def moove(self, screen, ai_settings):

        if (self.move_up is True) and (self.rect.top > screen.get_rect().top):
            self.y -= ai_settings.gun_speed_factor
        elif (self.move_down is True) and (self.rect.bottom < screen.get_rect().bottom):
            self.y += ai_settings.gun_speed_factor
        elif (self.move_left is True) and (self.rect.left > screen.get_rect().left):
            self.x -= ai_settings.gun_speed_factor
        elif (self.move_right is True) and (self.rect.right < screen.get_rect().right):
            self.x += ai_settings.gun_speed_factor

        self.rect.centerx = self.x
        self.rect.centery = self.y

    def rotate(self, screen, ai_settings) -> None:
        self.new_image = pygame.transform.rotate(self.image, self.angle)
        self.angle = (self.angle + self.rotate_direction) % 360
        self.rect = self.new_image.get_rect()
        self.rect.center = (self.x, self.y)

        screen.blit(self.new_image, self.rect)

    def centered(self):
        self.angle = 0
        self.x = self.screen_rect.centerx
        self.y = self.screen_rect.centery
