import pygame
from settings import Settings
from gun import Gun
import game_functions as gf
from pygame.sprite import Group
import random


def main():
    """иннициализируем всё и запускаем бесконечный цикл"""
    ai_settings = Settings()
    screen = pygame.display.set_mode(ai_settings.screen_size)
    gun = Gun(screen)
    bullets = Group()
    asteroids = Group()

    while True:
        screen.fill(ai_settings.color)
        gf.check_game_events(screen, ai_settings, gun, bullets, asteroids)

        #  TODO: переделать появление астероидов
        if random.randint(0, 60) == 0:
            gf.create_asteroid(screen, ai_settings, asteroids)

        gf.update_bullets(screen, bullets, asteroids)
        gf.update_asteroids(screen,  asteroids, gun)
        gf.update_screen(screen, ai_settings, gun, bullets, asteroids)


if __name__ == '__main__':
    pygame.init()
    main()
