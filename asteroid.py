from pygame.sprite import Sprite
import pygame
import random
import math


class Asteroid(Sprite):
    def __init__(self, screen):
        super(Asteroid, self).__init__()

        self.image = pygame.image.load('images/asteroid.png').convert()
        self.image.set_colorkey((255, 255, 255))
        self.screen = screen
        self.rect = self.image.get_rect()
        self.angle_degree = 0

        # задать стартовую позицию, направление и скорость
        self.x = float(random.randint(0, screen.get_rect().width))
        self.y = -float(self.rect.width) / 2
        self.angle_degree = -random.randint(60, 120)

    def update(self):
        self.x += 2 * math.cos(math.radians(self.angle_degree))
        self.y -= 2 * math.sin(math.radians(self.angle_degree))

        self.rect.centerx = self.x
        self.rect.centery = self.y

    def draw_asteroid(self, screen):
        screen.blit(self.image, self.rect)
