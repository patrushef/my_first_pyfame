import pygame


class Splash:
    def __init__(self, centerx, centery):
        self.image = pygame.image.load('images/splash.png').convert()
        self.rect = self.image.get_rect()
        self.image.set_colorkey((255, 255, 255))

        self.rect.centerx = centerx
        self.rect.centery = centery

    def draw_splash(self, screen):
        screen.blit(self.image, self.rect)

