import pygame
from pygame.sprite import Sprite
import math


class Bullet(Sprite):
    def __init__(self, screen, ball, ai_settings):
        super(Bullet, self).__init__()

        self.screen = screen
        self.rect = pygame.Rect(0, 0, ai_settings.bullet_width, ai_settings.bullet_height)

        self.x = float(ball.rect.centerx)
        self.y = float(ball.rect.centery)

        self.rect.centerx = self.x
        self.rect.centery = self.y
        self.color = ai_settings.bullet_color
        self.bullet_speed_factor = ai_settings.bullet_speed_factor
        self.angle_degree = ball.angle + 90

    def update(self):
        self.x += self.bullet_speed_factor * math.cos(math.radians(self.angle_degree))
        self.y -= self.bullet_speed_factor * math.sin(math.radians(self.angle_degree))

        self.rect.centerx = self.x
        self.rect.centery = self.y

    def draw_bullet(self):
        pygame.draw.rect(self.screen, self.color, self.rect)
